#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
int shsort(int s[], int n,int th)    
{
    int i,j,d;
    d=n/2;    /* Determine the fixed increase value */
	omp_set_num_threads(th);
    while(d>=1)
    {
	#pragma omp parallel sections
		{
#pragma omp section
        for(i=d+1;i<=n;i++)    /*Array subscripts perform direct insertion sorting from d + 1*/
        {
            s[0]=s[i];    /*Set up a surveillance whistle*/
            j=i-d;    /*Determine the rightmost position of the element to be compared*/
            while((j>0)&&(s[0]<s[j]))
            {
                s[j+d]=s[j];    /*right move*/
                j=j-d;    /*Move d position V*/
            }
            s[j + d]=s[0];    /*Insert the s [i] in the determined bits*/
        }
#pragma omp section
        d = d/2;    /*The increase becomes half of the original*/
    }
	}
return 0;
}

int main(int argc, char *argv[])
{
   int len,th=1;
	th=atoi(argv[1]);
	printf("array length:");
	scanf("%d",&len);
    int a[len];
    int i;
	printf("before sort:");
	for(i=1;i<=len;i++){
		a[i]=rand()%1000+1;
		
		printf(" %d ",a[i]);
	}
	printf("\n");
    shsort(a, len,th);    /* 调用 shsort()函数*/
   printf("after sort:");
    for(i=1;i<=len;i++)
        printf("%d  ",a[i]);
 
    printf("\n");
    return 0;
}
