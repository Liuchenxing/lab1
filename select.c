#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
void choice(int *array,int len,int th)
{
	omp_set_num_threads(th);
    for(int i = 0;i<len;i++)        
	{
		int tmp = i;         
#pragma omp parallel for
//#pragma omp master
		for(int j = i+1;j<len;j++)
		{
#pragma omp critical
		if(array[j]>array[tmp])    
			{
				tmp = j;     
			}
		}
//#pragma omp master
#pragma omp critical
		if(tmp != i)       
		{ 
			int temp = array[i];   
			array[i] = array[tmp];
			array[tmp] = temp;
		}
	}

}
int main(int argc, char *argv[])
{
	int len,th=1;
	th=atoi(argv[1]);
	printf("array length:");
	scanf("%d",&len);
    int array[len];
    int i;
	printf("before sort:");
	for(i=0;i<len;i++){
		array[i]=rand()%1000+1;
		
		printf(" %d ",array[i]);
	}
	printf("\n");
	
    choice(array,len,th);
	
	printf("after sort:");
    for(i=0;i<len;i++)
        printf(" %d ",array[i]);
 
    printf("\n");
    return 0;
}
